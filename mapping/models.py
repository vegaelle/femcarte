from django.contrib.gis.db import models

from djgeojson.fields import PointField

STATUS_CHOICES = (('draft', 'brouillon'),
                  ('active', 'actif'),
                  )

EVENT_STATUS_CHOICES = (('out', 'hors du système judiciaire'),
                        ('in', 'dans le système judiciaire'),
                        )


class Tag(models.Model):
    name = models.CharField('nom', max_length=50)
    description = models.TextField('description')

    def __str__(self):
        return self.name


class Point(models.Model):
    name = models.CharField('nom', max_length=50)
    coords = models.PointField('coordonnées')
    description = models.TextField('description', blank=True)
    add_date = models.DateTimeField('date d’ajout', auto_now_add=True)
    event_date = models.DateField('date de l’évènement')
    tags = models.ManyToManyField(Tag)
    status = models.CharField('statut', choices=STATUS_CHOICES, max_length=50)
    event_status = models.CharField('statut de l’évènement',
                                    choices=EVENT_STATUS_CHOICES,
                                    max_length=50)
    author_ip = models.GenericIPAddressField('adresse IP source', null=True)
    anonymous_submission = models.BooleanField('soumission anonyme',
                                               default=False)
    private_message = models.TextField('message complémentaire anonyme',
                                       null=True)
    concerned = models.BooleanField('l’auteur·e est la personne concernée',
                                    default=False)

    def __str__(self):
        return self.name


class Source(models.Model):
    point = models.ForeignKey(Point)
    title = models.CharField('titre', max_length=50)
    source_name = models.CharField('nom de la source', max_length=100)
    link = models.URLField('lien de la source', null=True)
    image = models.ImageField(upload_to='sources/', null=True)
    source_date = models.DateField('date de la publication')

    def __str__(self):
        return self.name
