from django.contrib.gis import admin

from .models import Source, Point, Tag


@admin.register(Point)
class PointAdmin(admin.ModelAdmin):
    list_display = ('name', 'event_date', 'add_date', 'anonymous_submission',
                    'author_ip', 'status')
    list_filter = ('event_date', 'add_date', 'anonymous_submission')
    exclude = ('anonymous_submission', 'author_ip')
    search_fields = ('name', 'description', 'tags__name')


@admin.register(Source)
class SourceAdmin(admin.ModelAdmin):
    list_display = ('title', 'source_name', 'source_date')
    list_filter = ('point', )
    search_fields = ('title', 'source_name')


admin.site.register(Tag)
